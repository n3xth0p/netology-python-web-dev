import requests

URL = 'http://127.0.0.1:5001/advertisement/'
READ_ADV_ID = 2
DELETE_ADV_ID = 12
DATA_ADV = {
    "owner": "петров",
    "title": "продаю пк 1323",
    "description": "продаю игровой пк, цена 25к"
}


def get_adv():
    response = requests.get(f'{URL}{READ_ADV_ID}')
    print(response.json())


def add_adv():
    response = requests.post(f'{URL}', json=DATA_ADV)
    print(response.json())


def delete_adv():
    response = requests.delete(f'{URL}{DELETE_ADV_ID}')
    print(response.text)


if __name__ == "__main__":
    get_adv()
    add_adv()
    delete_adv()
