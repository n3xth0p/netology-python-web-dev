from datetime import datetime
import json
import asyncio
import aiopg
from aiohttp import web
from gino import Gino
from asyncpg.exceptions import UniqueViolationError

DBNAME = 'aiohttp_final'
USER = 'postgres'
PASSWORD = 'Ciscocisco123'

db = Gino()
app = web.Application()
routes = web.RouteTableDef()
DSN = f'postgres://{USER}:{PASSWORD}@127.0.0.1:5432/{DBNAME}'


class BaseModel:

    @classmethod
    async def get_or_404(cls, obj_id):
        instance = await cls.get(obj_id)
        if instance:
            return instance
        raise web.HTTPNotFound(text='данного ID нет в базе')

    @classmethod
    async def create_instance(cls, **kwargs):
        try:
            instance = await cls.create(**kwargs)
        except UniqueViolationError:
            raise web.HTTPBadRequest()
        return instance


class Advertisements(db.Model, BaseModel):
    __tablename__ = 'advertise'

    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.String(100), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def to_dict(self):
        return {
            'id': self.id,
            'owner': self.owner,
            'title': self.title,
            'description': self.description,
            'created_at': str(self.created_at),
        }


async def set_connection():
    return await db.set_bind(DSN)


async def disconnect():
    return await db.pop_bind().close()


async def pg_pool(app):
    async with aiopg.create_pool(DSN) as pool:
        app['pg_pool'] = pool
        yield
        pool.close()


async def orm_engine(app):
    app['db'] = db
    await set_connection()
    await db.gino.create_all()
    yield
    await disconnect()


class AdvertisementView(web.View):

    async def get(self):
        adv_id = int(self.request.match_info['adv_id'])
        adv = await Advertisements.get_or_404(adv_id)
        return web.json_response(adv.to_dict())

    async def post(self):
        data = await self.request.json()
        adv_id = await Advertisements.create_instance(**data)
        print(adv_id.to_dict)

        return web.json_response(adv_id.to_dict())

    async def delete(self):
        adv_id = int(self.request.match_info['adv_id'])
        instance = await Advertisements.get_or_404(adv_id)
        await instance.delete()
        return web.Response(text=f'adv with id:{adv_id} deleted')


app.cleanup_ctx.append(orm_engine)
app.cleanup_ctx.append(pg_pool)

app.add_routes([
    web.get('/advertisement/{adv_id}', AdvertisementView),
    web.post('/advertisement/', AdvertisementView),
    web.delete('/advertisement/{adv_id}', AdvertisementView),
])

web.run_app(app, port=5001)
