import aiohttp
import asyncio
from more_itertools import chunked
from db_async import Character, db, PG_DSN
from config import MAX_REQUESTS
import time

BASE_URL = 'https://swapi.dev/api/people/'


async def get_pages(url):
    status_code, result = await make_request(url)
    counter = result.get('count')
    return counter


async def make_request(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            result = await response.json()
            status_code = response.status
    return status_code, result


async def parse_data(data, ind):
    result = {
        'id': ind,
        'name': data.get('name'),
        'birth_year': data.get('birth_year'),
        'eye_color': data.get('eye_color'),
        'films': ','.join(data.get('films')),
        'gender': data.get('gender'),
        'hair_color': data.get('hair_color'),
        'height': data.get('height'),
        'homeworld': data.get('homeworld'),
        'mass': data.get('mass'),
        'skin_color': data.get('skin_color'),
        'species': ','.join(data.get('species')),
        'starships': ','.join(data.get('starships')),
        'vehicles': ','.join(data.get('vehicles'))
    }
    return result


async def save_resource(url, ind):
    status_code, result = await make_request(f'{url}{ind}/')

    if status_code != 200:
        return False

    data = await parse_data(result, ind)
    character = await Character.create(**data)

    return character.to_dict()


async def main():
    await db.set_bind(PG_DSN)
    await db.gino.create_all()
    await Character.delete.gino.status()

    counter = await get_pages(BASE_URL)
    character_index = range(1, counter + 1)
    for index in chunked(character_index, MAX_REQUESTS):
        list_character_tasks = [asyncio.create_task(save_resource(BASE_URL, ind)) for ind in index]
        await asyncio.gather(*list_character_tasks)

    await db.pop_bind().close()


if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    start_time = time.time()
    asyncio.run(main())
    print("--- %s seconds ---" % (time.time() - start_time))
