from gino import Gino
from config import USERNAME, PASSWORD, DBNAME

PG_DSN = f"postgresql://{USERNAME}:{PASSWORD}@127.0.0.1:5432/{DBNAME}"

db = Gino()

class Character(db.Model):
    __tablename__ = "character"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=100), nullable=True)
    birth_year = db.Column(db.String(length=10), nullable=True)
    eye_color = db.Column(db.String(length=20), nullable=True)
    films = db.Column(db.Text, nullable=True)
    gender = db.Column(db.String(length=20), nullable=True)
    hair_color = db.Column(db.String(length=20), nullable=True)
    height = db.Column(db.String, nullable=True)
    homeworld = db.Column(db.String(255), nullable=True)
    mass = db.Column(db.String(length=10), nullable=True)
    skin_color = db.Column(db.String(length=20), nullable=True)
    species = db.Column(db.Text, nullable=True)
    starships = db.Column(db.Text, nullable=True)
    vehicles = db.Column(db.Text, nullable=True)
