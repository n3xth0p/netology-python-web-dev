import sqlalchemy as sq
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DSN = 'postgresql://postgres:Ciscocisco123@127.0.0.1:5432/netology_sw_sync'
engine = sq.create_engine(DSN)
Base = declarative_base()
Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)

class Character(Base):
    '''id - ID персонажа
    birth_year
    eye_color
    films - строка с названиями фильмов через запятую
    gender
    hair_color
    height
    homeworld
    mass
    name
    skin_color
    species - строка с названиями типов через запятую
    starships - строка с названиями кораблей через запятую
    vehicles - строка с названиями транспорта через запятую
    '''

    __tablename__ = "character"

    id = sq.Column(sq.Integer, primary_key=True)
    name = sq.Column(sq.String(length=100), nullable=True)
    birth_year = sq.Column(sq.String(length=10), nullable=True)
    eye_color = sq.Column(sq.String(length=20), nullable=True)
    films = sq.Column(sq.Text, nullable=True)
    gender = sq.Column(sq.String(length=20), nullable=True)
    hair_color = sq.Column(sq.String(length=20), nullable=True)
    height = sq.Column(sq.String, nullable=True)
    homeworld = sq.Column(sq.String(255), nullable=True)
    mass = sq.Column(sq.String(length=10), nullable=True)
    skin_color = sq.Column(sq.String(length=20), nullable=True)
    species = sq.Column(sq.Text, nullable=True)
    starships = sq.Column(sq.Text, nullable=True)
    vehicles = sq.Column(sq.Text, nullable=True)


def create_schema():
    Base.metadata.create_all(bind=engine)


def save_character(item: dict):
    create_schema()
    session = Session()

    query = Character(birth_year=item.get('birth_year'),
                      eye_color=item.get('eye_color'),
                      films=item.get('films'),
                      gender=item.get('gender'),
                      hair_color=item.get('hair_color'),
                      height=item.get('height'),
                      homeworld=item.get('homeworld'),
                      mass=item.get('mass'),
                      name=item.get('name'),
                      skin_color=item.get('skin_color'),
                      species=item.get('species'),
                      starships=item.get('starships'),
                      vehicles=item.get('vehicles'))

    session.add(query)
    session.commit()
    session.close()


