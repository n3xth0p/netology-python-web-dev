import requests
import time
from draft.db import save_character

API_URL = f'https://swapi.dev/api/people/'

def parse_characters(character):
    print(character)
    for key in ['films', 'homeworld', 'starships', 'vehicles']:
        char_list = list()
        try:
            # print(key)
            # print(character[key])
            # print(type(character[key]))
            if not isinstance(character[key], str):
                # print(key)
                # print(type(character[key]))
                character[key] = ','.join(character[key])
        except KeyError:
            # print(key, character)
            character[key] = None

    print(character)
    return character

def get_character(url):
    r = requests.get(url)
    resp = r.json()
    return parse_characters(r.json())

if __name__ == "__main__":
    url = f"https://swapi.dev/api/people/"
    full_characters = []
    # print(get_pages(API_URL))
    # r = requests.get(url)
    #
    # # pprint(r.json())
    #
    start_time = time.time()
    people = f"https://swapi.dev/api/people/"
    r = requests.get(people)
    # counter = get_pages(url)
    counter = r.json()['count']
    # full_characters.extend(parse_characters(r.json()['results']))

    for i in range(1, counter + 1):
        url = f"https://swapi.dev/api/people/{i}"
        # r = requests.get(url)
        # resp = r.json()
        # char_to_db = parse_characters(r.json())
        char_to_db = get_character(url)
        full_characters.extend(char_to_db)
        # pprint(full_characters)
        save_character(char_to_db)

    # pprint(full_characters)
    print("--- %s seconds ---" % (time.time() - start_time))
