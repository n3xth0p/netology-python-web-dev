from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import config
from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__)
app.debug = True
app.config.from_mapping(SQLALCHEMY_DATABASE_URI=config.POSTGRE_URI)
db = SQLAlchemy(app)
app.config['SECRET_KEY'] = 'Ciscocisco123'
toolbar = DebugToolbarExtension(app)
