
ADVERT_CREATE = {
	"type": "object",
	"properties": {
		"owner": {
			"type": "string"
		},
		"title": {
			"type": "string",
		},

		"description": {
			"type": "string",
		}
	},
	"required": ["owner", "title", "description"]
}