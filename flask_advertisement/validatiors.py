import jsonschema
from flask import request, make_response, jsonify
import errors

ADVERT_CREATE = {
    "type": "object",
    "properties": {
        "owner": {
            "type": "string"
        },
        "title": {
            "type": "string",
        },

        "description": {
            "type": "string",
        }
    },
    "required": ["owner", "title", "description"]
}


def validate(req_schema):
    """Валидатор входящих запросов"""

    def decorator(func):

        def wrapper(*args, **kwargs):
            try:
                jsonschema.validate(
                    request.get_json(), schema=req_schema,
                )
            except jsonschema.ValidationError as e:
                raise errors.BadLuck

            result = func(*args, **kwargs)

            return result

        return wrapper

    return decorator
