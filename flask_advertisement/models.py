from app import db
import errors
from sqlalchemy import exc
import datetime


class BaseModelMixin:

    @classmethod
    def get_by_id(cls, obj_id):
        obj = cls.query.get(obj_id)
        if obj:
            return obj
        else:
            raise errors.NotFound

    @classmethod
    def del_by_id(cls, obj_id):
        obj = cls.query.get(obj_id)
        if obj:
            db.session.delete(obj)
            db.session.commit()

    def add(self):
        db.session.add(self)
        try:
            db.session.commit()
        except exc.IntegrityError:
            raise errors.BadLuck





class Advertisement(db.Model, BaseModelMixin):
    """Объвления"""

    __tablename__ = "advertise"
    __table_args__ = (db.UniqueConstraint('owner', 'title', name='cs_1'),)

    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.String(64), index=True)
    title = db.Column(db.String(100), index=True)
    description = db.Column(db.Text)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)


    def to_dict(self):
        return {
            'id': self.id,
            'owner': self.owner,
            'title': self.title,
            'description': self.description,
            'created_at': self.created_at
        }

db.create_all()
