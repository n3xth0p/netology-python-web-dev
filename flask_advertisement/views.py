from flask.views import MethodView
from models import Advertisement
from flask import request, jsonify
from app import app
from errors import BasicException
from validatiors import validate, ADVERT_CREATE


class AdvertisementView(MethodView):

    def get(self, adv_id):
        if adv_id:
            adv = Advertisement.get_by_id(adv_id)
            return jsonify(adv.to_dict())
        else:
            return BasicException

    @validate(ADVERT_CREATE)
    def post(self):
        adv = Advertisement(**request.json)
        adv.add()
        return jsonify(adv.to_dict())

    def delete(self, adv_id):
        if adv_id:
            adv = Advertisement.del_by_id(adv_id)
            return jsonify()
        else:
            return BasicException


app.add_url_rule('/advertisement/<int:adv_id>', view_func=AdvertisementView.as_view('adv_get'), methods=['GET', ])
app.add_url_rule('/advertisement/', view_func=AdvertisementView.as_view('adv_add'), methods=['POST', ])
app.add_url_rule('/advertisement/<int:adv_id>', view_func=AdvertisementView.as_view('adv_del'), methods=['DELETE', ])